import algorithms.fibonacci.*;
import algorithms.utils.DurationTimer;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Tooltip;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;

public class Main extends Application {
    /**
     * The upper bound of the fibonacci numbers which will
     * be calculated.
     */
    private static final int UPPER_BOUND = 30;

    /**
     * Number of warmup iterations
     */
    private static final int NUMBER_OF_WARMUP_ITERATIONS = 15;

    /**
     * The thread counts which should be used for the calculations.
     */
    private static final List<Integer> THREAD_COUNTS = new ArrayList<>(Arrays.asList(2, 4, 8, 12, 16, 24, 32));

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Fibonacci DNC Comparision");
        XYChart<Number, Number> chart = getChart();
        Scene scene = new Scene(chart, 800, 600);
        primaryStage.setScene(scene);
        primaryStage.show();


        for (XYChart.Series<Number, Number> s : chart.getData()) {
            float avgms = 0;
            for (XYChart.Data<Number, Number> d : s.getData()) {
                Tooltip tooltip = new Tooltip(String.format("fib(%d) = %d in %.2fms%n%s", d.getXValue().intValue(), d.getExtraValue(), d.getYValue().doubleValue(), s.getName()));
                Tooltip.install(d.getNode(), tooltip);
                avgms += d.getYValue().floatValue();
            }
            avgms /= s.getData().size();
            s.setName(s.getName() + String.format("(∅ %.2fms)", avgms));
        }
    }

    public static void main(String[] args) {
        launch(args);
    }

    private static XYChart<Number, Number> getChart() {
        NumberAxis xAxis = new NumberAxis();
        xAxis.setLabel("Fibonacci Number");
        xAxis.setAutoRanging(false);
        xAxis.setLowerBound(0);
        xAxis.setUpperBound(UPPER_BOUND);
        xAxis.setTickUnit(1.0);

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Execution time [ms]");

        for (int i = 0; i < NUMBER_OF_WARMUP_ITERATIONS; i++) {
            System.out.println("Warmup Iteration #" + (i + 1) + " of " + NUMBER_OF_WARMUP_ITERATIONS);
            getDynamicSeries();
            getSimpleSeries();
            getLinearSeries();
            getNaïveSeries();
            getThreadedSeries();
        }

        System.out.println("Running benchmark now:");

        LineChart<Number, Number> chart = new LineChart<>(xAxis, yAxis);
        chart.setTitle("Fibonacci DNC Comparison");
        chart.getData().add(getDynamicSeries());
        chart.getData().add(getSimpleSeries());
        chart.getData().add(getNaïveSeries());
        chart.getData().add(getLinearSeries());
        chart.getData().addAll(getThreadedSeries());

        return chart;
    }

    private static Series getLinearSeries() {
        System.out.println("\tCalculating with fancy math...");
        DurationTimer timer = new DurationTimer();
        Series series = new Series();
        series.setName("Fancy Math Linear");
        for (long i = 0; i <= UPPER_BOUND; i++) {
            timer.start();
            long result = FibonacciConstant.fib(i);
            timer.stop();
            series.getData().add(new XYChart.Data<>(i, timer.getDuration(), result));
        }

        return series;
    }

    private static Series getNaïveSeries() {
        System.out.println("\tCalculating with Naïve programming...");
        DurationTimer timer = new DurationTimer();
        Series series = new Series();
        series.setName("No DNC");
        for (long i = 0; i <= UPPER_BOUND; i++) {
            timer.start();
            long result = FibonacciNaïve.fib(i);
            timer.stop();
            series.getData().add(new XYChart.Data<>(i, timer.getDuration(), result));
        }

        return series;
    }

    private static Series getDynamicSeries() {
        System.out.println("\tCalculating with Dynamic programming...");
        DurationTimer timer = new DurationTimer();
        Series series = new Series();
        series.setName("Dynamic DNC");
        for (long i = 0; i <= UPPER_BOUND; i++) {
            timer.start();
            FibonacciDynamicDNC fib = new FibonacciDynamicDNC(i);
            long result = fib.divideAndConquer(new ConcurrentHashMap<>());
            timer.stop();
            series.getData().add(new XYChart.Data<>(i, timer.getDuration(), result));
        }

        return series;
    }

    private static Series getSimpleSeries() {
        System.out.println("\tCalculating with simple DnC programming...");
        DurationTimer timer = new DurationTimer();
        Series series = new Series();
        series.setName("Simple DNC");
        for (long i = 0; i <= UPPER_BOUND; i++) {
            timer.start();
            FibonacciDNC fib = new FibonacciDNC(i);
            long result = fib.divideAndConquer();
            timer.stop();
            series.getData().add(new XYChart.Data(i, timer.getDuration(), result));
        }

        return series;
    }

    private static List<Series<Number, Number>> getThreadedSeries() {
        List<Series<Number, Number>> allSeries = new ArrayList<>();

        for (int threadCount : THREAD_COUNTS) {
            ForkJoinPool pool = new ForkJoinPool(threadCount);

            System.out.printf("\tCalculating with %d threads...%n", threadCount);
            DurationTimer timer = new DurationTimer();
            Series series = new Series();
            series.setName(String.format("Threaded DNC (%d threads)", threadCount));
            for (long i = 0; i <= UPPER_BOUND; i++) {
                timer.start();
                FibonacciDNCThreaded fib = new FibonacciDNCThreaded(i);
                long result = fib.divideAndConquer(pool, threadCount);
                timer.stop();
                series.getData().add(new XYChart.Data(i, timer.getDuration(), result));
            }
            allSeries.add(series);
        }
        return allSeries;
    }
}
