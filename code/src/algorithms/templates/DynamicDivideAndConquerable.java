package algorithms.templates;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

/**
 * Sub interface which uses concurrent hash maps to save the previous results to recycle them.
 */

public interface DynamicDivideAndConquerable<InputType, OutputType> extends DivideAndConquerable<OutputType> {
    default OutputType divideAndConquer(AbstractMap<InputType, OutputType> cache) {
        if (this.isBasic()) {
            return this.baseFun();
        }

        // Check if in cache
        if (cache.containsKey(this.getCacheKey())) {
            return cache.get(this.getCacheKey());
        }

        List<? extends DynamicDivideAndConquerable<InputType, OutputType>> subcomponents = this.decompose();
        List<OutputType> intermediateResults = new ArrayList<>(subcomponents.size());
        subcomponents.forEach(subcomponent -> intermediateResults.add(subcomponent.divideAndConquer(cache)));


        OutputType recombinedIntermediateResult = recombine(intermediateResults);
        // Put into cache
        cache.put(this.getCacheKey(), recombinedIntermediateResult);
        return recombinedIntermediateResult;
    }

    List<? extends DynamicDivideAndConquerable<InputType, OutputType>> decompose();

    InputType getCacheKey();
}
