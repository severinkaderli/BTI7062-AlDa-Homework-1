package algorithms.fibonacci;

public class FibonacciNaïve {
    public static Long fib(Long input) {
        if (input == 0 || input == 1) {
            return input;
        } else {
            return fib(input - 1L) + fib(input - 2L);
        }
    }
}
