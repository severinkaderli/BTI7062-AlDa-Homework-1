package algorithms.fibonacci;

import algorithms.templates.DivideAndConquerable;

import java.util.ArrayList;
import java.util.List;

/**
 * An implementation to calculate the fibonacci numbers using the
 * simple Divide and Conquer interface.
 */
public class FibonacciDNC implements DivideAndConquerable<Long> {
    Long input;

    public FibonacciDNC(Long input) {
        this.input = input;
    }

    @Override
    public boolean isBasic() {
        return this.input == 0 || this.input == 1;
    }

    @Override
    public Long baseFun() {
        return this.input;
    }

    @Override
    public List<? extends DivideAndConquerable<Long>> decompose() {
        List<FibonacciDNC> decomposedInput = new ArrayList<>();
        decomposedInput.add(new FibonacciDNC(this.input - 1));
        decomposedInput.add(new FibonacciDNC(this.input - 2));
        return decomposedInput;
    }

    @Override
    public Long recombine(List<Long> intermediateResults) {
        return intermediateResults.stream().mapToLong(Long::valueOf).sum();
    }

}
