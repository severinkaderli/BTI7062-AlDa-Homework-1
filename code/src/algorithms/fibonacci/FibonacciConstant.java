package algorithms.fibonacci;

import java.math.BigDecimal;

/**
 * Uses some fancy math to unwrap fibonacci.
 * Only accurate to fib(1074) because double can't hold larger numbers
 */
public class FibonacciConstant {
    public static Long fib(Long input) {
        Double n = Double.valueOf(input);
        Double a = (1.0 + Math.sqrt(5.0))/2;
        Double b = (1.0 - Math.sqrt(5.0))/2;
        a = Math.pow(a, n);
        b = Math.pow(b, n);
        return (long)(1/Math.sqrt(5) * (a -b));
    }
}
