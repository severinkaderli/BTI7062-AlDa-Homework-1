package algorithms.fibonacci;

import algorithms.templates.ThreadedDivideAndConquerable;

import java.util.ArrayList;
import java.util.List;

/**
 * An implementation to calculate the fibonacci numbers using the
 * threaded Divide and Conquer sub interface.
 */

public class FibonacciDNCThreaded implements ThreadedDivideAndConquerable<Long> {
    Long input;

    public FibonacciDNCThreaded(Long input) {
        this.input = input;
    }

    @Override
    public boolean isThreadable() {
        return this.input > 7;
    }

    @Override
    public boolean isBasic() {
        return this.input == 0 || this.input == 1;
    }

    @Override
    public Long baseFun() {
        return this.input;
    }

    @Override
    public List<? extends ThreadedDivideAndConquerable<Long>> decompose() {
        List<FibonacciDNCThreaded> decomposedInput = new ArrayList<>();
        decomposedInput.add(new FibonacciDNCThreaded(this.input - 1));
        decomposedInput.add(new FibonacciDNCThreaded(this.input - 2));
        return decomposedInput;
    }

    @Override
    public Long recombine(List<Long> intermediateResults) {
        return intermediateResults.stream().mapToLong(Long::valueOf).sum();
    }
}
