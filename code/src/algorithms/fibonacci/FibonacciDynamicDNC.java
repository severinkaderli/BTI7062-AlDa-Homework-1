package algorithms.fibonacci;

import algorithms.templates.DynamicDivideAndConquerable;

import java.util.ArrayList;
import java.util.List;

/**
 * An implementation to calculate the fibonacci numbers using the
 * dynamic Divide and Conquer sub interface.
 */

public class FibonacciDynamicDNC implements DynamicDivideAndConquerable<Long, Long> {
    Long input;

    public FibonacciDynamicDNC(Long input) {
        this.input = input;
    }

    @Override
    public boolean isBasic() {
        return this.input == 0 || this.input == 1;
    }

    @Override
    public Long baseFun() {
        return this.input;
    }

    @Override
    public List<? extends DynamicDivideAndConquerable<Long, Long>> decompose() {
        List<FibonacciDynamicDNC> decomposedInput = new ArrayList<>();
        decomposedInput.add(new FibonacciDynamicDNC(this.input - 1));
        decomposedInput.add(new FibonacciDynamicDNC(this.input - 2));
        return decomposedInput;
    }

    @Override
    public Long recombine(List<Long> intermediateResults) {
        return intermediateResults.stream().mapToLong(Long::valueOf).sum();
    }

    @Override
    public Long getCacheKey() {
        return this.input;
    }




}
