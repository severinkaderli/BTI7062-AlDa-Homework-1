package algorithms.fibonacci;

import org.junit.Test;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;

import static org.junit.Assert.assertEquals;

public class FibonacciThreadedDNCTest {

    @Test
    public void testFibonacci() {
        FibonacciDNCThreaded fib = new FibonacciDNCThreaded(5L);
        assertEquals(Long.valueOf(5), fib.divideAndConquer(new ForkJoinPool(2), 2));

        fib = new FibonacciDNCThreaded(6L);
        assertEquals(Long.valueOf(8), fib.divideAndConquer(new ForkJoinPool(256), 256));

        fib = new FibonacciDNCThreaded(21L);
        assertEquals(Long.valueOf(10946), fib.divideAndConquer(new ForkJoinPool(8), 8));
    }

}