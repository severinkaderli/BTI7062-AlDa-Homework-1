package algorithms.fibonacci;

import org.junit.Test;

import static org.junit.Assert.*;

public class FibonacciNaïveTest {
    @Test
    public void testFibonnaci() {

        assertEquals(Long.valueOf(5), FibonacciNaïve.fib(5L));
        assertEquals(Long.valueOf(8), FibonacciNaïve.fib(6L));
        assertEquals(Long.valueOf(10946), FibonacciNaïve.fib(21L));
    }
}