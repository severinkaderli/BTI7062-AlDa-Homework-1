package algorithms.fibonacci;

import org.junit.Test;

import static org.junit.Assert.*;

public class FibonacciDNCTest {
    @Test
    public void testFibonnaci() {
        FibonacciDNC fib = new FibonacciDNC(5L);

        assertEquals(Long.valueOf(5), fib.divideAndConquer());

        fib = new FibonacciDNC(6L);
        assertEquals(Long.valueOf(8), fib.divideAndConquer());

        fib = new FibonacciDNC(21L);
        assertEquals(Long.valueOf(10946), fib.divideAndConquer());
    }
}