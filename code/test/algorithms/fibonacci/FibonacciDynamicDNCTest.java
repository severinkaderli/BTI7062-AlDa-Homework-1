package algorithms.fibonacci;

import org.junit.Test;

import java.util.concurrent.ConcurrentHashMap;

import static org.junit.Assert.assertEquals;

public class FibonacciDynamicDNCTest {

    @Test
    public void testFibonacci() {
        FibonacciDynamicDNC fib = new FibonacciDynamicDNC(5L);
        assertEquals(Long.valueOf(5), fib.divideAndConquer(new ConcurrentHashMap<>()));

        fib = new FibonacciDynamicDNC(6L);
        assertEquals(Long.valueOf(8), fib.divideAndConquer(new ConcurrentHashMap<>()));

        fib = new FibonacciDynamicDNC(21L);
        assertEquals(Long.valueOf(10946), fib.divideAndConquer(new ConcurrentHashMap<>()));
    }

}