package algorithms.fibonacci;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FibonacciFancyMathTest {
    @Test
    public void testFibonnaci() {
        assertEquals(Long.valueOf(5), FibonacciConstant.fib(5L));

        assertEquals(Long.valueOf(8), FibonacciConstant.fib(6L));

        assertEquals(Long.valueOf(10946), FibonacciConstant.fib(21L));
    }
}