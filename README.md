# BTI7062-AlDa-Homework-1
The homework was to create different interfaces and implementations of the  Divide and Conquer algorithm for the Fibonacci series, and to compare their execution times.

## ZIP File
The ready to submit zip file can be found [here](https://gitlab.com/severinkaderli/BTI7062-AlDa-Homework-1/builds/artifacts/master/raw/TEAM-Kaderli-Kilic-Schaer.zip?job=deploy).

## Used DNC types
* Simple DNC
* Threaded DNC
* Dynamic DNC